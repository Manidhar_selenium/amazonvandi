package Com.rediff.pro;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import Com.rediff.uills.Common;
import Com.rediff.uills.ExcelReder;


public class C2  extends Common {
	@BeforeMethod
	public void openbrowser() throws IOException {
		LoadFiles();
		lodFile2();
		openBrowser();		
		
	
	}
	@Test (priority=1)
	public void varifypagetitle() throws IOException{
		
		SoftAssert softasserction=new  SoftAssert();
		try {
		
		String expectedtitle="Rediff.com: News | Rediffmail | Stock Quotes | Shopping";
		String actualtitle=driver.getTitle();
		//HARD ASSERCTION 
		//Assert.assertEquals(actualtitle, expectedtitle);
		
		/////SOFT ASSERCTION\\\\
		
		
		softasserction.assertEquals(actualtitle, expectedtitle);
		System.out.println("vafified");
		softasserction.assertEquals(actualtitle, expectedtitle,"title");
		softasserction.assertEquals(actualtitle, expectedtitle,"asdfghjkl");
		softasserction.assertEquals(actualtitle, expectedtitle,"qwertyuiop");
		softasserction.assertAll();
		}catch(Exception e) {
			CaptureScreenShot();
			e.printStackTrace();
			softasserction.assertAll();
		}
	}
		//dependson method
		//@Test(priority=2,dependsOnMethods="varifypagetitle")
		//enabled=true,false
		//@Test(priority=2,dependsOnMethods="varifypagetitle",enabled=false)
		//how many times we want to run =invocation Count
	
		@Test(priority=2,dependsOnMethods="varifypagetitle",enabled=true,invocationCount=1,dataProvider="Testdata")


	public   void invalidloigin(String email,String password ) throws InterruptedException, IOException {
			SoftAssert softasserction=new  SoftAssert();
try {
		
		//3.click on sign in link
		driver.findElement(By.linkText(objects.getProperty("signinlinktext"))).click();
		//4.Enter invalid usre name as sdfghj@uyj
		driver.findElement(By.id(objects.getProperty("Emailid"))).sendKeys(email);
		//5.Enter invalid passowerd as ghjklgh
		driver.findElement(By.id(objects.getProperty("Enterpassword"))).sendKeys(password);
		//6.click on sign in button
		driver.findElement(By.name(objects.getProperty("Clickonsubmit"))).click();
		//7.Validate the error message
		String actualerrormessage=driver.findElement(By.id(objects.getProperty("ValidateError"))).getText();
		String expectederrormessage="Wrong username and password combination.";
	    System.out.println("actualerrormessage: "+ actualerrormessage);
	    System.out.println("expectederrormessage: "+expectederrormessage);
	/*    if(actualerrormessage.equals(expectederrormessage)) {
	    System.out.println("error message mached");
	    }else {
	    	throw new Error("error message not mached");
	    }*/
	  
	    softasserction.assertEquals(actualerrormessage, expectederrormessage);
}catch(Exception e){
	CaptureScreenShot();
	
}
	    Thread.sleep(7000);
	    
	    JavascriptExecutor r=(JavascriptExecutor)driver;
	    r.executeScript("scroll(0,500)");
	    
	    
	}
	
	@AfterMethod
	public void closebrowser(){
		closeBrowser();
	}
	@DataProvider
	public Object[][] Testdata() throws EncryptedDocumentException, IOException {

	return new ExcelReder().readexceldata();
	
	}
	
}
